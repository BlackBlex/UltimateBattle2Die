using Godot;
using System;
using GodotCSTools;

namespace UltimateBattle2Die.Autoloads
{
    public class TransitionManager : CanvasLayer
    {
        [Signal]
        delegate void TransitionCompleted();

        [NodePath("Background")]
        private Panel _background;
        [NodePath("Animation")]
        private Tween _animation;

        private TransitionMType _type;

        private string _path;

        private Node _element;
        private Node _currentScene;

        private float _defaultDuration = 1;
        private float _duration = -1;

        private float _defaultDelay = 0;
        private float _delay = -1;

        public void Fade(Node scene, Node element)
        {
            setElement(scene, element);
            effectFade();
        }
        public void Fade(string path)
        {
            setScene(path);
            effectFade();
        }

        async public void FadeIn(Node scene, Node element)
        {
            setElement(scene, element);
            effectFadeIn();
            await ToSignal(_animation, "tween_completed");
            activator();
            reset();
        }
        async public void FadeIn(string path)
        {
            setScene(path);
            effectFadeIn();
            await ToSignal(_animation, "tween_completed");
            activator();
            reset();
        }

        async public void FadeOut(Node scene, Node element)
        {
            setElement(scene, element);
            effectFadeOut();
            await ToSignal(_animation, "tween_completed");
            activator();
            reset();
        }
        async public void FadeOut(string path)
        {
            setScene(path);
            effectFadeOut();
            await ToSignal(_animation, "tween_completed");
            activator();
            reset();
        }

        public void SetDuration(float value)
        {
            _duration = value;
        }
        public void SetDelay(float value)
        {
            _delay = value;
        }



        private void activateAnimation(System.Object original, System.Object final, Godot.Object obj, NodePath property)
        {
            float duration = _duration == -1f ? _defaultDuration : _duration;
            float delay = _delay == -1f ? _defaultDelay : _delay;
            
            _animation.InterpolateProperty(obj, property, original, final, duration, Tween.TransitionType.Quad, Tween.EaseType.InOut, delay);
            _animation.Start();
        }

        private void activator()
        {
            if ( _type != TransitionMType.None )
            {
                switch(_type)
                {
                    case TransitionMType.Scene:
                        if ( !_path.Empty() )
                        {
                            GetTree().ChangeScene(_path);
                            _path = "";
                        }
                        break;
                    case TransitionMType.Element:
                        if ( _element != null && _currentScene != null )
                        {
                            _currentScene.AddChild(_element);
                            _element = null;
                            _currentScene = null;
                        }
                        break;
                }
            }
            _type = TransitionMType.None;
        }

        async private void effectFade()
        {
            _background.Visible = true;
            effectFadeIn(true);
            await ToSignal(_animation, "tween_completed");
            activator();
            effectFadeOut(true);
            await ToSignal(_animation, "tween_completed");
            EmitSignal("TransitionCompleted");
            _background.Visible = false;
            reset();
        }

        async private void effectFadeIn(bool fullEffect = false)
        {
            if ( !fullEffect )
                _background.Visible = true;
            activateAnimation(new Color(1,1,1,0), new Color(1,1,1,1), _background, "modulate");
            if ( !fullEffect )
            {
                await ToSignal(_animation, "tween_completed");
                _background.Visible = false;
            }
        }

        async private void effectFadeOut(bool fullEffect = false)
        {
            if ( !fullEffect )
                _background.Visible = true;
            activateAnimation(new Color(1,1,1,1), new Color(1,1,1,0), _background, "modulate");
            if ( !fullEffect )
            {
                await ToSignal(_animation, "tween_completed");
                _background.Visible = false;
            }
        }

        private void reset()
        {
            _delay = -1;
            _duration = -1;
        }

        private void setElement(Node scene, Node element)
        {
            _currentScene = scene;
            _element = element;
            _type = TransitionMType.Element;
        }
        private void setScene(string path)
        {
            _path = path;
            _type = TransitionMType.Scene;
        }

        private enum TransitionMType
        {
            Scene,
            Element,
            None
        }



        public override void _Ready()
        {
            this.SetupNodeTools();
        }

    }

}
