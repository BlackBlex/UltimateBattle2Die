using Godot;
using System;
using UltimateBattle2Die.Autoloads;
using GodotCSTools;

namespace UltimateBattle2Die.Menus
{
    public class SplashScreen : Node
    {
        public override void _Ready()
        {
            TransitionManager transition = this.GetSingleton<TransitionManager>("TransitionManager");
            if ( transition != null )
            {
                transition.SetDuration(4);
                //transition.SetDelay(1);
                transition.Fade("res://src/menus/Main.tscn");
            }
            else
                throw new Exception("TransitionManager not load");
        }
    }
}