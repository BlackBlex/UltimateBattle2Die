using Godot;
using System;
using UltimateBattle2Die.Autoloads;
using GodotCSTools;

namespace UltimateBattle2Die.Menus
{
    public class Main : Node
    {
        [NodePath("Camera")]
        private Camera2D _camera;

        [NodePath("BandTop")]
        private ParallaxBackground _band_top;
        [NodePath("BandTop/Top")]
        private ParallaxLayer _wave_up;
        [NodePath("BandTop/Top/Image")]
        private Sprite _wave_up_sprite;
        
        [NodePath("BandDown")]
        private ParallaxBackground _band_down;
        [NodePath("BandDown/Down")]
        private ParallaxLayer _wave_down;
        [NodePath("BandDown/Down/Image")]
        private Sprite _wave_down_sprite;

        [NodePath("Frame/Background/Actions")]
        private VBoxContainer _actions;

        private Viewport _viewport;

        [NodePath("Frame/Background")]
        private Panel _menu;
        private Theme _menu_theme;

        //Band variables
        private string[] _band_colors = {
            "#ff5c44", //#Red
            "#59c05d", //#Green
            "#fcc84e", //#Yellow
            "#c059ad" //#Purple
        };

        private float _band_delta;

        private Color _band_old_color;

        private Color _band_color_target;

        private int _band_color_index;

        private readonly static float BAND_TARGET_TIME = 2.5f;
        //Band variables


        private readonly static Vector2 WAVE_IMAGE_SIZE = new Vector2(1024, 92);
        private readonly static Vector2 WINDOW_SIZE_MIN = new Vector2(1024, 600);



        private void applyStyleBoxTexture()
        {
            StyleBoxTexture style = _menu_theme.GetStylebox("normal", "Button") as StyleBoxTexture;
            if ( style != null )
            {
                style.ModulateColor = _band_old_color.LinearInterpolate(_band_color_target, 0.4f);
                _menu_theme.SetStylebox("normal", "Button", style as StyleBoxTexture);
            }
        }

        private void changeColorBands(float delta)
        {
            _band_delta += delta;
            if ( _band_delta > BAND_TARGET_TIME ) 
            {
                _band_color_index++;
                _band_delta = 0f;
            }

            if ( _band_color_index > (_band_colors.Length - 1) ) 
                _band_color_index = 0;

            _band_color_target = new Color(_band_colors[_band_color_index]);
            _band_old_color = _wave_up_sprite.GetModulate();
            Color newColor = _band_old_color.LinearInterpolate(_band_color_target, _band_delta);
            _wave_up_sprite.SetModulate(newColor);
            _wave_down_sprite.SetModulate(newColor);

            applyStyleBoxTexture();
        }

        private void checkWindowSizeMin()
        {
            Vector2 currentSize = OS.GetWindowSize();

            if ( currentSize.x < WINDOW_SIZE_MIN.x )
                OS.SetWindowSize(new Vector2(WINDOW_SIZE_MIN.x, currentSize.y));
            
            if ( currentSize.y < WINDOW_SIZE_MIN.y )
                OS.SetWindowSize(new Vector2(currentSize.x, WINDOW_SIZE_MIN.y));

        }

        private void processKeyboard()
        {
            if ( Input.IsActionPressed("ui_cancel") )
                GetTree().Quit();
        }

        private void setParallaxSize()
        {
            Vector2 currentSize = OS.GetWindowSize();
            float divider = (float) currentSize.x / (float) WAVE_IMAGE_SIZE.x;
            
            _wave_up.MotionMirroring = new Vector2(WAVE_IMAGE_SIZE.x * Mathf.Ceil(divider), _wave_up.MotionMirroring.y);
            _wave_up_sprite.RegionRect = new Rect2(new Vector2(0,0), new Vector2(WAVE_IMAGE_SIZE.x * Mathf.Ceil(divider), WAVE_IMAGE_SIZE.y));

            _wave_down.MotionMirroring = new Vector2(WAVE_IMAGE_SIZE.x * Mathf.Ceil(divider), _wave_down.MotionMirroring.y);
            _wave_down_sprite.RegionRect = new Rect2(new Vector2(0,0), new Vector2(WAVE_IMAGE_SIZE.x * Mathf.Ceil(divider), WAVE_IMAGE_SIZE.y));

            _band_down.Offset = new Vector2(0, currentSize.y);
        }



        public override void _Ready()
        {
            this.SetupNodeTools();
            
            _viewport = this.GetViewport();
            _viewport?.Connect("size_changed", this, "OnMainResized");
            _menu_theme = _menu.GetTheme();

            setParallaxSize();
        }

       public override void _Process(float delta)
       {
           changeColorBands(delta);
           processKeyboard();
           Vector2 cameraPosition = _camera.Position;
           cameraPosition.x += 5;
           _camera.Position = cameraPosition;
       }



        //Signals callbacks
        public void OnButtonPlayPressed()
        {
            TransitionManager transition = this.GetSingleton<TransitionManager>("TransitionManager");
            if ( transition != null )
            {
                transition.SetDuration(2);
                transition.SetDelay(1);
                //transition.Fade("res://src/menu/Lobby.tscn");
            }
            else
                throw new Exception("TransitionManager not load");
        }

        public void OnMainResized()
        {
            checkWindowSizeMin();
            setParallaxSize();
        }
        //Signals callbacks
    }
}
