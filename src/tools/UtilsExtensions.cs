using System;
using System.Collections.Generic;
using System.Linq;
using Godot;

namespace UltimateBattle2Die.Tools
{
    public static class UtilsExtensions
    {
        public static IEnumerable<Enum> GetFlags(this Enum input)
        {
            foreach (Enum value in Enum.GetValues(input.GetType()))
                if (input.HasFlag(value))
                    yield return value;
        }

        public static float Average(this Vector2 vec)
        {
            return ( vec.x + vec.y ) / 2;
        }
        
        public static bool IsType(Type t, Type target)
        {
            if (t == target) return true;   
            if (t == typeof(Godot.Node) ) return false;
            return IsType(t.BaseType, target);
        }
    }
}
