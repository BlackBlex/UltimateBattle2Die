using EntityFramework.Core.Interfaces.System;
using Godot;
using System;
using System.Collections.Generic;

namespace EntityFramework
{
    public class SystemManager : Node, IInitializeSystem, IProcessSystem, ICleanupSystem
    {
        protected readonly List<IInitializeSystem> _initSystem;
        protected readonly List<IProcessSystem> _processSystem;
        protected readonly List<ICleanupSystem> _cleanupSystem;

        public SystemManager()
        {
            _initSystem = new List<IInitializeSystem>();
            _processSystem = new List<IProcessSystem>();
            _cleanupSystem = new List<ICleanupSystem>();
        }

        public virtual SystemManager Add(ISystem system)
        {
            AddOrRemove(system);
            return this;
        }

        public virtual SystemManager Remove(ISystem system)
        {
            AddOrRemove(system, true);
            return this;
        }

		public void Initialize()
		{
            _initSystem.ForEach(
                (init) => init.Initialize()
            );
            /*foreach ( IInitializeSystem init in _initSystem )
                init.Initialize();*/
		}

		public void Cleanup()
		{
            _cleanupSystem.ForEach(
                (clean) => clean.Cleanup()
            );
            /*foreach ( ICleanupSystem clean in _cleanupSystem )
                clean.Cleanup();*/
		}

		public void Process(float delta)
		{
            _processSystem.ForEach(
                (process) => process.Process(delta)
            );
            /*foreach ( IProcessSystem process in _processSystem )
                process.Process(delta);*/
		}

        public void ActivateReactiveSystem()
        {
            _processSystem.ForEach(
                (process) => {
                    IReactiveSystem reactive = process as IReactiveSystem;
                    if ( reactive != null )
                        reactive.Activate();
                    
                    SystemManager nestedSystem = process as SystemManager;
                    if ( nestedSystem != null )
                        nestedSystem.ActivateReactiveSystem();
                }
            );
            /*foreach ( IProcessSystem process in _processSystem )
            {
                IReactiveSystem reactive = process as IReactiveSystem;
                if ( reactive != null )
                    reactive.Activate();
                
                SystemManager nestedSystem = process as SystemManager;
                if ( nestedSystem != null )
                {
                    nestedSystem.ActivateReactiveSystem();
                }
            }*/
        }

        public void DeactivateReactiveSystem()
        {
            _processSystem.ForEach(
                (process) => {
                    IReactiveSystem reactive = process as IReactiveSystem;
                    if ( reactive != null )
                        reactive.Deactivate();
                    
                    SystemManager nestedSystem = process as SystemManager;
                    if ( nestedSystem != null )
                        nestedSystem.DeactivateReactiveSystem();
                }
            );
            /*foreach ( IProcessSystem process in _processSystem )
            {
                IReactiveSystem reactive = process as IReactiveSystem;
                if ( reactive != null )
                    reactive.Deactivate();
                
                SystemManager nestedSystem = process as SystemManager;
                if ( nestedSystem != null )
                {
                    nestedSystem.DeactivateReactiveSystem();
                }
            }*/
        }

        public void ClearReactiveSystem()
        {
            _processSystem.ForEach(
                (process) => {
                    IReactiveSystem reactive = process as IReactiveSystem;
                    if ( reactive != null )
                        reactive.Clear();
                    
                    SystemManager nestedSystem = process as SystemManager;
                    if ( nestedSystem != null )
                        nestedSystem.ClearReactiveSystem();
                }
            );
            /*foreach ( IProcessSystem process in _processSystem )
            {
                IReactiveSystem reactive = process as IReactiveSystem;
                if ( reactive != null )
                    reactive.Clear();
                
                SystemManager nestedSystem = process as SystemManager;
                if ( nestedSystem != null )
                {
                    nestedSystem.ClearReactiveSystem();
                }
            }*/
        }

        private void AddOrRemove(ISystem system, bool remove = false)
        {
            var initializeSystem = system as IInitializeSystem;
            if ( initializeSystem != null )
                if ( !remove )
                    _initSystem.Add(initializeSystem);
                else
                    _initSystem.Remove(initializeSystem);

            var processSystem = system as IProcessSystem;
            if ( processSystem != null )
                if ( !remove )
                    _processSystem.Add(processSystem);
                else
                    _processSystem.Remove(processSystem);
                
            var cleanupSystem = system as ICleanupSystem;
            if ( cleanupSystem != null )
                if ( !remove )
                    _cleanupSystem.Add(cleanupSystem);
                else
                    _cleanupSystem.Remove(cleanupSystem);
        }
    }
}
