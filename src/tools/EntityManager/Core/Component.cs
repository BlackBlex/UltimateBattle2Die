using Godot;
using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace EntityFramework.Core
{
	public class Component : Node2D, IComponent
	{
		public Entity Parent { get; set; }

		public Action<Type, Type, Entity> PropertyUpdated;

		public virtual void Start(){}

		public virtual void Process(float delta){}

		public virtual void End(){}

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {			
			PropertyUpdated?.Invoke(this.GetType().GetInterfaces().Where( t => !t.Name.Contains("INotifyPropertyChanged") && !t.Name.Contains("IComponent") && !t.Name.Contains("IDisposable")).First() , Parent.GetType(), Parent);
        }

	}
}
