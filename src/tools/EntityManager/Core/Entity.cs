using Godot;
using System;
using System.Collections.Generic;
using GodotCSTools;
using System.Reflection;
using UltimateBattle2Die.Components;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MoreLinq;

namespace EntityFramework.Core
{

#region Entity
    public partial class Entity : KinematicBody2D
    {
        public EntityManager Manager { get; set; }

        public long UniqueId { get; set; }

        [NodePath("Collision")]
        public CollisionShape2D Collision { get; set; }

        [NodePath("Image")]
        public Sprite Image { get; set; }
        [NodePath("AnimatedImage")]
        public AnimatedSprite AnimatedImage { get; set; }
        [NodePath("SpriteStack")]
        public Node2D SpriteStack { get; set; }

        public override void _Ready()
        {
            this.SetupNodeTools();

            if ( Manager == null )
                Manager = this.GetSingleton<EntityManager>("EntityManager");
            
            if ( UniqueId <= 0)
            {
                UniqueId = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                Manager.PendingEntities = 1;
                ProcessEntity();
            }
            
            ProcessComponents();
        }

        public override void _PhysicsProcess(float delta)
        {
            _components.ForEach(
                (comp) => 
                    comp.Process(delta)
            );
            /*foreach ( Component comp in _components )
                comp.Process(delta);*/
        }

        public void ActiveCollision()
        {
            Collision.Disabled = false;
        }

        public void DisableCollision()
        {
            Collision.Disabled = true;
        }

		public override bool Equals(object o)
		{
            Entity entity = o as Entity;
            if ( entity != null )
                return UniqueId == entity.UniqueId;
            
            return false;
		}

		public override int GetHashCode()
		{
            return UniqueId.GetHashCode();
		}
    }
#endregion

#region Procesamiento del Entity
    public partial class Entity
    {
        public virtual void Start()
        {
            _components.ForEach(
                (comp) =>
                    comp.Start()
            );
            /*foreach ( Component comp in _components)
                comp.Start();*/
        }

        public virtual void Clean() {}

        private void ProcessEntity()
        {
            if ( Manager.ActiveEntities.Get(UniqueId) == null )
            {
                CallDeferred("Reparent");
            }
        }

        private void Reparent()
        {
            GetParent().RemoveChild(this);
            Manager.ActiveEntities.Add(this);
            Manager.PendingEntities = -1;
        }

        public override string ToString()
        {
            return string.Format("Entity{{{0}}}", UniqueId);
        }
    }
#endregion

#region Sistema de Componentes del Entity
    public partial class Entity
    {
        private List<Component> _components = new List<Component>();
        private List<Node> _componentsNode = new List<Node>();

        public T GetComponentNode<T>() where T : Node
        {
            return _componentsNode.Find( comp => comp as T != null ) as T;
        }

        public Node GetComponentNode(string name, bool baseType = false)
        {
            if ( !baseType )
                return _componentsNode.Find( comp => comp.Name.Contains(name) );
            else
                return _componentsNode.Find( comp => comp.GetType().Name.Contains(name) );
        }

        public T GetComponent<T>() where T : class
        {
            if ( !typeof(T).IsInterface )
                throw new InvalidCastException();

            return _components.Find( comp => comp as T != null ) as T;
        }

        public Component GetComponentFromString(string name, bool baseType = false)
        {
            Component comp = null;

            if ( baseType )
            {
                comp = _components.Find( component => 
                    component.GetType().GetInterface(name) != null
                );
            }
            else
                comp = _components.Find( component => component.Name.Contains(name) );

            return comp;
        }

        public void ProcessComponents()
        {
            _components.Clear();
            _componentsNode.Clear();

            foreach ( Node nodeChild in this.GetNode("Components").GetChildren() )
            {
                Component comp = nodeChild as Component;
                if ( comp != null )
                {
                    comp.Parent = this;
                    comp.PropertyUpdated -= Manager.AddEntityChanges;
                    comp.PropertyUpdated += Manager.AddEntityChanges;
                    comp.Start();
                    _components.Add(comp);
                }
                else
                {
                    IComponentNode compNode = nodeChild as IComponentNode;

                    if ( compNode != null )
                    {
                        compNode.Parent = this;
                        _componentsNode.Add(nodeChild);
                        nodeChild._Ready();
                    }
                }
            }
        }
    }
#endregion

/*#region IEqualityComparer
	public class EntityEqualityComparer<T> : IEqualityComparer<T> where T : Entity
	{
		public bool Equals(T x, T y)
		{
            return x.UniqueId == y.UniqueId;
		}

		public int GetHashCode(T obj)
		{
            return obj.UniqueId.GetHashCode();
		}
	}
#endregion*/

#region NotifyPropertyChanged
	public partial class Entity
	{        
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
			Manager.AddEntityChangesString(propertyName, GetType(), this);
        }
	}
#endregion

#region Metodos modificados
    public partial class Entity
    {
        public new void MoveAndSlide(Vector2 linear, Vector2? floor = null, float slope = 5, int maxBounces = 4, float floorMax = 0.785398f)
        {
            base.MoveAndSlide(linear, floor, slope, maxBounces, floorMax);
            OnPropertyChanged("Position");
        }

        public new void QueueFree()
        {
            _components.ForEach( 
                (comp) => 
                    comp.End()
            );
            /*foreach ( Component comp in _components )
                comp.End();*/
            
            Manager.Remove(this);
        }
    }
#endregion
}
