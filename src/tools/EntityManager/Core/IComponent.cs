namespace EntityFramework.Core
{
    public interface IComponent
    {
        void Start();
        void Process(float delta);
        void End();
    }

    public interface IComponentNode
    {
		Entity Parent { get; set; }
    }
}
