using System;
using System.Collections.Generic;
using System.Linq;
using EntityFramework.Core.Interfaces;

namespace EntityFramework.Core
{
	public class Collector<TEntity> : ICollector<TEntity> where TEntity : Entity
	{
		public HashSet<TEntity> collectedEntities { get { return _collectedEntities; } }

		public int Count { get { return _collectedEntities.Count; } }

        readonly HashSet<TEntity> _collectedEntities;

        private Type _componentType;
        private String _componentTypeString;

        private Action<Type, Type, Entity> _addEntityCache;
        private Action<String, Type, Entity> _addEntityCacheString;

        private EntityManager _manager;

        public Collector(String componentType, EntityManager manager) : this(manager)
        {
            _componentTypeString = componentType;
            _addEntityCacheString = addEntityString;
            Activate();
        }

        public Collector(Type componentType, EntityManager manager) : this(manager)
        {
            _componentType = componentType;
            _addEntityCache = addEntity;

            Activate();
        }

        public Collector(EntityManager manager)
        {
            _manager = manager;
            _collectedEntities = new HashSet<TEntity>();
        }

		public void Activate()
		{
            if ( _addEntityCache != null )
            {
                _manager.AddEntityCollector -= _addEntityCache;
                _manager.AddEntityCollector += _addEntityCache;
            }
            if ( _addEntityCacheString != null )
            {
                _manager.AddEntityCollectorString -= _addEntityCacheString;
                _manager.AddEntityCollectorString += _addEntityCacheString;
            }
		}

		public void ClearCollectedEntities()
		{
            _collectedEntities.Clear();
		}

		public void Deactivate()
		{
            if ( _addEntityCache != null )
                _manager.AddEntityCollector -= _addEntityCache;
            
            if ( _addEntityCacheString != null )
                _manager.AddEntityCollectorString -= _addEntityCacheString;
            ClearCollectedEntities();
		}

		public IEnumerable<TCast> GetCollectedEntities<TCast>() where TCast : Entity
		{
            return _collectedEntities.Cast<TCast>();
		}

        private void addEntity(Type componentType, Type entityType, Entity entity)
        {
            if ( _componentType != componentType )
                return;

            var newEntity = entity as TEntity;
            if ( newEntity != null )
                _collectedEntities.Add(newEntity);
        }

        private void addEntityString(String componentType, Type entityType, Entity entity)
        {
            if ( _componentTypeString != componentType )
                return;

            var newEntity = entity as TEntity;
            if ( newEntity != null )
                _collectedEntities.Add(newEntity);
        }

        ~Collector()
        {
            Deactivate();
        }
	}
}
