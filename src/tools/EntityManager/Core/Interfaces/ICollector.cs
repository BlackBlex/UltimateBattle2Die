using System.Collections.Generic;

namespace EntityFramework.Core.Interfaces
{
    public interface ICollector
    {
        int Count { get; }

        void Activate();
        void Deactivate();
        void ClearCollectedEntities();

        IEnumerable<TCast> GetCollectedEntities<TCast>() where TCast : Entity;
    }

    public interface ICollector<TEntity> : ICollector where TEntity : Entity {

        HashSet<TEntity> collectedEntities { get; }
    }
}
