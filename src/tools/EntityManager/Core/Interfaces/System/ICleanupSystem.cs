namespace EntityFramework.Core.Interfaces.System
{
    public interface ICleanupSystem : ISystem 
    {
        void Cleanup();
    }
}
