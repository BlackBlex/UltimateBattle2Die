namespace EntityFramework.Core.Interfaces.System
{
    public interface IReactiveSystem : IProcessSystem
    {
        void Activate();
        void Deactivate();
        void Clear();
    }
}
