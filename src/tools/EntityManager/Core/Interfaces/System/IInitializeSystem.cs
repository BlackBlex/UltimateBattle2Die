namespace EntityFramework.Core.Interfaces.System
{
    public interface IInitializeSystem : ISystem 
    {
        void Initialize();
    }
}
