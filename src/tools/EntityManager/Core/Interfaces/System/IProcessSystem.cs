namespace EntityFramework.Core.Interfaces.System
{
    public interface IProcessSystem : ISystem
    {
        void Process(float delta);
    }
}
