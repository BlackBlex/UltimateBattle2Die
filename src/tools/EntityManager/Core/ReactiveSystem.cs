using System;
using System.Collections.Generic;
using EntityFramework.Core.Interfaces;
using EntityFramework.Core.Interfaces.System;
using Godot;
using System.Linq;

namespace EntityFramework.Core
{
	public abstract class ReactiveSystem<TEntity> : IReactiveSystem where TEntity : Entity
	{
        readonly List<TEntity> _buffer;
        protected readonly EntityManager _manager;

        readonly ICollector<TEntity> _collector;

        protected ReactiveSystem(EntityManager manager)
        {
            _buffer = new List<TEntity>();
            _manager = manager;
            if ( ComponentType() != null )
                _collector = new Collector<TEntity>(ComponentType(), manager);

            if ( ComponentTypeString() != null )
                _collector = new Collector<TEntity>(ComponentTypeString(), manager);
        }

        protected virtual Type ComponentType() { return null; }
        protected virtual String ComponentTypeString() { return null; }

        protected abstract Func<TEntity, bool> Filter();

        protected abstract void Process(float delta, List<TEntity> entities);

        public void Activate() { _collector.Activate(); }

        public void Deactivate() { _collector.Deactivate(); }

        public void Clear() { _collector.ClearCollectedEntities(); }

		public void Process(float delta)
		{
            if ( _collector.Count > 0 )
            {
                var entities = _collector.collectedEntities.Where(Filter());

                if ( entities.Count() > 0)
                    _buffer.AddRange(entities);

                /*_collector.collectedEntities.ToList().ForEach(
                    (entity) =>
                    {
                        if ( Filter(entity) )
                        {
                            _buffer.Add(entity);
                        }
                    }
                );*/

                /*foreach ( TEntity entity in _collector.collectedEntities )
                {
                    if ( Filter(entity) )
                        _buffer.Add(entity);
                }*/
          
                _collector.ClearCollectedEntities();

                if ( _buffer.Count > 0 )
                {
                    Process(delta, _buffer);
                    _buffer.Clear();
                }
            }
		}
	}
}
