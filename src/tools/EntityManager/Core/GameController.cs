using EntityFramework;
using Godot;
using GodotCSTools;

namespace EntityFramework.Core
{
    public class GameController : Node
    {    
        protected SystemManager _systems;
        protected EntityManager _manager;
        
        private bool started = false;

        public void Start()
        {
            _systems.Initialize();
            started = true;
        }

        public override void _Ready()
        {
            _systems = new SystemManager();
            _manager = this.GetSingleton<EntityManager>("EntityManager");
        }

        public override void _Process(float delta)
        {
            if ( started )
            {
                _systems.Process(delta);
                _systems.Cleanup();
            }
        }

    }
}
