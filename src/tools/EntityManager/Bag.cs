using System;
using EntityFramework.Core;
using Godot;
using UltimateBattle2Die.Tools;
using System.Linq;

namespace EntityFramework
{
    public class Bag : Node2D
    {
        public int Count
        { 
            get;
            private set;
        }

        public bool IsEmpty
        {
            get
            {
                return this.Count == 0;
            }
        }

        public Dictionary<string, int> Types
        { 
            get
            {
                return typesOfElement;
            }
        }
     
        private Dictionary<String, int> typesOfElement = new Dictionary<String, int>();
   
        public void Add<T>(T element) where T : Node
        {
            string nameElement = element.GetType().Name;
            if ( !typesOfElement.ContainsKey(nameElement) )
                typesOfElement.Add(nameElement, 0);
            
            ++typesOfElement[nameElement];
            
            base.AddChild(element, true);
            ++this.Count;
        }

        public bool Contains<T>(string element, bool baseClass = false) where T : Node
        {
            return Get<T>(element, baseClass) != null;
        }

        public bool Contains<T>(T element, bool baseClass = false) where T : Node
        {
            return Contains<T>(element.Name, baseClass);
        }

        public int GetCountType<T>() where T : Node
        {
            int result = 0;

            string nameElement = typeof(T).Name;
            if ( typesOfElement.ContainsKey(nameElement) )
                result = typesOfElement[nameElement];

            return result;
        }

        public T Get<T>() where T : Node
        {
            T result = base.GetChildren().Where(
                (e) => UtilsExtensions.IsType(e.GetType(), typeof(T))
            ).FirstOrDefault() as T;
            /*bool correct;

            foreach ( Node element in base.GetChildren() )
            {
                correct = UtilsExtensions.IsType(element.GetType(), typeof(T));

                if ( correct )
                {
                    result = element as T;
                    break;
                }
            }*/
            return result;
        }

        public T Get<T>(string element, bool baseClass = false) where T : Node
        {
            bool correct = false;
            T result = null;
            
            foreach( Node node in base.GetChildren() )
            {
                correct = false;
                if ( node.Name.Contains(element) )
                {
                    if ( node.GetType() == typeof(T) )
                        correct = true;
                    
                    if ( !correct && baseClass )
                    {
                        if ( UtilsExtensions.IsType(node.GetType(), typeof(T)) )
                            correct = true;
                    }

                    if ( correct )
                    {
                        result = node as T;
                        break;
                    }
                }
            }

            if ( correct )
                return result;
            else
                return null;
        }

        public Entity Get(long id)
        {
            Entity result = base.GetChildren().Where(
                (e) => 
                    (e as Entity).UniqueId == id
            ).FirstOrDefault() as Entity; //null;
            
            /*foreach ( Node element in base.GetChildren() )
            {
                result = element as Entity;

                if ( result != null )
                {
                    if ( result.UniqueId == id )
                        break;
                }
            }*/
            return result;
        }

        public Godot.Array GetAll()
        {
            return base.GetChildren();
        }

        public T Remove<T>(string element) where T : Node
        {
            T result = Get<T>(element);

            if ( result != null )
                if ( !Remove<T>(result) )
                    result = default(T);

            return result;
        }

        public bool Remove<T>(T element, bool baseClass = false) where T : Node
        {
            if ( Contains<T>(element, baseClass) )
            {
                string nameElement = element.GetType().Name;
                --typesOfElement[nameElement];
                if ( typesOfElement[nameElement] == 0 )
                    typesOfElement.Remove(nameElement);
                base.RemoveChild(element);
                --this.Count;
                return true;
            }

            return false;
        }

        public bool RemoveAll<T>(bool baseClass = false) where T : Node
        {
            bool isResult = false;
            bool remove;
            foreach ( Node element in base.GetChildren() )
            {
                remove = false;

                if ( element.GetType() == typeof(T) )
                    remove = true;

                if ( !remove && baseClass )
                    remove = UtilsExtensions.IsType(element.GetType(), typeof(T));

                if ( remove )
                {
                    if ( this.Remove<T>(element as T, baseClass) )
                        isResult = true;
                }
            }
            return isResult;
        }


        [Obsolete("This is not supported in this class.\nYou must use Add(T Element)", true)]
        public new void AddChild(Node node, bool legibleUniqueName = false)
        {
            throw new NotImplementedException("Don't use!!");
        }

        [Obsolete("This is not supported in this class.\nYou must use Add(T Element)", true)]
        public new void GetNode(NodePath node)
        {
            throw new NotImplementedException("Don't use!!");
        }

        [Obsolete("This is not supported in this class.\nYou must use Remove(T Element)", true)]
        public new void RemoveChild(Node node)
        {
            throw new NotImplementedException("Don't use!!");
        }

    }
}
