using EntityFramework.Core;
using Godot;
using System;
using GodotCSTools;
using System.Collections.Generic;
using System.Threading.Tasks;
using UltimateBattle2Die.Tools;
using System.Linq;

namespace EntityFramework
{

#region EntityManager
    public partial class EntityManager : Node
    {
        [NodePath("ActiveEntities")]
        public Bag ActiveEntities { get; set; }
        [NodePath("RemovedEntities")]
        public Bag RemovedEntities { get; set; }

        public override void _Ready()
        {
            this.SetupNodeTools();
            #region Ver Sistema de debug
            _debugInfo.Add(DebugLocation.TOP, _debugInfoTop);
            _debugInfo.Add(DebugLocation.RIGHT, _debugInfoRight);
            _debugInfo.Add(DebugLocation.BOTTOM, _debugInfoBottom);
            _debugInfo.Add(DebugLocation.LEFT, _debugInfoLeft);
            timing = defaultTime;
            #endregion
        }

        public override void _Process(float delta)
        {
            #region Ver Procesamiento de Entities
            if ( !_alertSend && _pendingEntities == 0 )
            {
                EmitSignal("AllEntitiesProcessed");
                _alertSend = true;
            }
            #endregion
            
            #region Ver Sistema de debug
            ResetMessages(DebugLocation.TOP | DebugLocation.RIGHT | DebugLocation.LEFT);
            ResetBottomMessages(delta);
            DebugInfoEntities();
            #endregion
        }
    }
#endregion

#region Metodos del EntityManager
    public partial class EntityManager
    {
        public void Add<T>(T element) where T : Entity
        {
            if ( element.UniqueId <= 0 )
                element.UniqueId = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);

            ActiveEntities.Add<T>(element);
        }

        public T Create<T>() where T : Entity
        {
            T result = RemovedEntities.Get<T>();
            
            if ( result == null )
            {
                string urlTscn = typeof(T).FullName;
                urlTscn = urlTscn.Replace(urlTscn.Split(".")[0], "res://src");
                urlTscn = urlTscn.Replace(".", "/");
                urlTscn += ".tscn";
                result = (ResourceLoader.Load(urlTscn) as PackedScene).Instance() as T;
                result.UniqueId = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
            }
            else
            {
                RemovedEntities.Remove<T>(result);
                result.Start();
                result.ActiveCollision();
                result.SetProcess(true);
                result.SetPhysicsProcess(true);
            }

            ActiveEntities.Add<T>(result);

            return result;
        }

        public T Create<T>(string path) where T : Entity
        {
            T result = (GD.Load(path) as PackedScene).Instance() as T;

            bool contains = RemovedEntities.Contains(result);

            if ( !contains )
            {
                result.UniqueId = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
            }
            else
            {
                result = RemovedEntities.Get<T>(result.Name);
                RemovedEntities.Remove<T>(result);
                result.Start();
                result.ActiveCollision();
                result.SetProcess(true);
                result.SetPhysicsProcess(true);
            }

            ActiveEntities.Add<T>(result);

            return result;
        }

        public int GetCountType<T>() where T : Entity
        {
            return ActiveEntities.GetCountType<T>();
        }

        public List<T> GetEntities<T>() where T : Entity
        {
            List<T> entities = new List<T>();
            int totalEntities = ActiveEntities.GetCountType<T>();
            
            if ( totalEntities > 0 )
            {
                Godot.Array allEntities = ActiveEntities.GetAll();
                
                var entitiesType = allEntities.Where(
                    (e) => 
                        UtilsExtensions.IsType((e).GetType(), typeof(T))
                ).Cast<T>();

                entities.AddRange(entitiesType);

                /*bool correct = false;
                foreach ( Node entity in allEntities )
                {
                    correct = UtilsExtensions.IsType(entity.GetType(), typeof(T));

                    if ( correct )
                        entities.Add(entity as T);
                }*/
            }

            return entities;
        }

        public T GetEntity<T>(long uniqueId) where T : Entity
        {
            return ActiveEntities.Get(uniqueId) as T;
        }

        public bool IsActive(long uniqueId)
        {
            return ActiveEntities.Get(uniqueId) != null;
        }

        public void Remove(Entity entity)
        {
            if ( ActiveEntities.Remove(entity, true) )
            {
                RemovedEntities.Add(entity);
            }

            entity.SetProcess(false);
            entity.SetPhysicsProcess(false);
            entity.DisableCollision();
            entity.Position = new Vector2(5000,5000);
            entity.Clean();
        }
    }
#endregion

#region Procesamiento de Entities
    public partial class EntityManager
    {
        public int PendingEntities 
        {
            get { return _pendingEntities; }
            set { 
                if ( _alertSend && value > 0 )
                    _alertSend = false;
                _pendingEntities += value;
            }
        }

        private int _pendingEntities = 0;

        private bool _alertSend = false;

        [Signal]
        delegate void AllEntitiesProcessed();

        public async Task<bool> WaitForEntitiesProcessed()
        {
            await ToSignal(this, "AllEntitiesProcessed");
            return true;
        }

		public Action<Type, Type, Entity> AddEntityCollector;
		public Action<String, Type, Entity> AddEntityCollectorString;

        public void AddEntityChanges(Type componentType, Type entityType, Entity entity)
        {
            if ( AddEntityCollector != null)
                AddEntityCollector(componentType, entityType, entity);
        }

        public void AddEntityChangesString(String componentType, Type entityType, Entity entity)
        {
            if ( AddEntityCollectorString != null)
                AddEntityCollectorString(componentType, entityType, entity);
        }
    }
#endregion

#region Sistema de debug
    public partial class EntityManager
    {
        [Flags]
        public enum DebugLocation
        {
            TOP = 1,
            RIGHT = 1 << TOP,
            BOTTOM = 1 << RIGHT,
            LEFT = 1 << BOTTOM,

            ALL = TOP | RIGHT | BOTTOM | LEFT
        }

        [NodePath("DebugHud/DebugInfoTop")]
        private Label _debugInfoTop;
        [NodePath("DebugHud/DebugInfoRight")]
        private Label _debugInfoRight;
        [NodePath("DebugHud/DebugInfoBottom")]
        private Label _debugInfoBottom;
        [NodePath("DebugHud/DebugInfoLeft")]
        private Label _debugInfoLeft;

        private Godot.Dictionary<DebugLocation, Label> _debugInfo = new Godot.Dictionary<DebugLocation, Label>();

        private string _debugStringEntities = "";

        private float defaultTime = 10f;
        private float timing;

        public void AddMessage(DebugLocation loc, String msg)
        {
            foreach ( DebugLocation l in loc.GetFlags() )
            {
                if ( l == DebugLocation.ALL )
                    continue;
                _debugInfo[l].Text += msg;
            }
        }

        public string GetTextDebugInfo(DebugLocation loc)
        {
            string result = "";
            foreach ( DebugLocation l in loc.GetFlags() )
            {
                if ( l == DebugLocation.ALL )
                    continue;
                result += _debugInfo[l].Text;
            }
            return result;
        }

        public void ResetMessages(DebugLocation loc)
        {
            foreach ( DebugLocation l in loc.GetFlags() )
            {
                if ( l == DebugLocation.ALL )
                    continue;
                _debugInfo[l].Text = "";
            }
        }

        private void DebugInfoEntities()
        {
            _debugStringEntities = "Active Entities: " + ActiveEntities.Count + "\nTypes actives:";

            foreach ( KeyValuePair<string, int> type in ActiveEntities.Types )
            {
                _debugStringEntities += "\n    " + type.Key + ": " + type.Value;
            }
            _debugStringEntities += "\nRemoved Entities: " + RemovedEntities.Count + "\nTypes removed:";
            
            foreach ( KeyValuePair<string, int> type in RemovedEntities.Types )
            {
                _debugStringEntities += "\n    " + type.Key + ": " + type.Value;
            }

            AddMessage(DebugLocation.LEFT, _debugStringEntities);
        }

        private void ResetBottomMessages(float delta)
        {
            if( GetTextDebugInfo(DebugLocation.BOTTOM) != "" )
            {
                timing -= delta;
                if ( timing <= 0 )
                {
                    ResetMessages(DebugLocation.BOTTOM);
                    timing = defaultTime;
                }
            }
        }
    }
#endregion

}
