using EntityFramework.Core;
using Godot;
using GodotCSTools;
using UltimateBattle2Die.Components;
using UltimateBattle2Die.Core.Interfaces;

namespace UltimateBattle2Die.Entities.Projectiles
{
	public class Projectile : Entity
    {
        public Entity PlayerOwner;
        [Export]
		public float Damage = 5f;
        [Export]
		public float DamageMultiplier = 1f;
        [Export]
		public float Speed = 600f;

        private Vector2 _velocity;

        private Delay LifeTime;

        private bool hitMade = false;

        public override void Start()
        {
            base.Start();
            hitMade = false;
            Collision.Disabled = false;
            LifeTime.SetWaitTime(LifeTime.DelayTime);
            LifeTime.Start();
        }

        public override void Clean()
        {
            _velocity = new Vector2(0,0);
            LifeTime.Stop();
        }

        public void Setup(Vector2 pos, Vector2 dir, Entity own)
        {
            Position = pos;
            Rotation = dir.Angle();
            _velocity = dir * Speed;
            PlayerOwner = own;
        }

        public override void _Ready()
        {
            base._Ready();
            
            Collision = this.GetNode<CollisionShape2D>("Area/Collision");

            LifeTime = GetComponentNode("LifeTime") as Delay;

            IDamageDealer damageDealer = GetComponent<IDamageDealer>();
            if ( damageDealer != null )
                damageDealer.DealDamage = Damage;
        }

        public override void _PhysicsProcess(float delta)
        {
            base._PhysicsProcess(delta);

            if ( _velocity != new Vector2(0,0) )
                MoveAndSlide(_velocity);
        }
        
        public void OnAreaBodyEntered(Godot.Object body)
        {
            if ( !hitMade )
            {
                hitMade = true;
                Collision.Disabled = true;
                Entity bodyEntity = body as Entity;
                if ( bodyEntity != null )
                {
                    IHittable hit = bodyEntity.GetComponent<IHittable>();
                    if ( hit != null )
                        hit.Hit(this);
                }

                CallDeferred("QueueFree");
            }
        }

        public void OnLifeTimeTimeout()
        {
            CallDeferred("QueueFree");
        }
    }
}
