using Godot;
using UltimateBattle2Die.Entities.Characters;
using GodotCSTools;
using EntityFramework.Core;
using UltimateBattle2Die.Core.Interfaces;
using UltimateBattle2Die.Components;

namespace UltimateBattle2Die.Entities.Items
{
	public class ItemBase : Entity
    {
        [NodePath("PointOrigin")]
        public Position2D PointOrigin { get; set; }

        public Delay Delay { get; set; }

        [Export]
		public Vector2 Offset = new Vector2(45,0);
        
        [Export]
		public int Range = 45;

        protected IPickable Pickable { get; set; }

        public override void _Ready()
        {
            base._Ready();

            Pickable = GetComponent<IPickable>();
            
            if ( Pickable != null )
            {
                Pickable.Offset = Offset;
                Pickable.Range = Range;
            }

            Collision = this.GetNode<CollisionShape2D>("Area/Collision");
            (Collision.GetShape() as CircleShape2D).SetRadius(Range);
        }

        public void OnAreaBodyEntered(Godot.Object body)
        {
            Entity bodyEntity = body as Entity;
            if ( bodyEntity != null )
            {
                if ( bodyEntity is Character )
                {
                    if ( Pickable != null )
                        Pickable.ObjOwner = bodyEntity;
                }
            }
        }

        public void OnAreaBodyExited(Godot.Object body)
        {
            if ( Pickable != null )
            {
                if ( !Pickable.IsPicked )
                {
                    Pickable.ObjOwner = null;
                }
            }
        }
    }
}
