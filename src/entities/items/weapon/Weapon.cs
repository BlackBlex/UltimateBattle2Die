using Godot;
using System;
using GodotCSTools;
using UltimateBattle2Die.Entities.Projectiles;
using UltimateBattle2Die.Components;

namespace UltimateBattle2Die.Entities.Items.Weapon
{
    public class Weapon : ItemBase
    {
        [NodePath("ImageDrop")]
        public Sprite ImageDrop { get; set; }
        [NodePath("AnimatedImageDrop")]
        public AnimatedSprite AnimatedImageDrop;

        [Export]
        public PackedScene Bullet = null;

        protected bool _canShoot = true;

        public void Use()
        {
            if ( _canShoot )
            {
                Projectile newBullet = Manager.Create<Projectile>(Bullet.ResourcePath);

                if ( newBullet != null )
                {
                    Vector2 direction = new Vector2(1, 0).Rotated(PointOrigin.GlobalRotation);
                    newBullet.Setup(PointOrigin.GlobalPosition, direction, Pickable.ObjOwner);
                    _canShoot = false;
                }
            }
        }

        public override void _Ready()
        {
            base._Ready();

            Delay = GetComponentNode<Delay>();
            Delay.Start();
        }

        public override void _PhysicsProcess(float delta)
        {
            base._PhysicsProcess(delta);

            if ( Pickable != null )
            {
                if ( !Pickable.IsPicked )
                {
                    if ( ImageDrop.Visible || Image.Visible ) 
                    {
                        ImageDrop.Visible = true;
                        Image.Visible = false;
                    }
                    else if ( AnimatedImageDrop.Visible || AnimatedImage.Visible )
                    {
                        AnimatedImageDrop.Visible = true;
                        AnimatedImage.Visible = false;
                    }
                }
                else
                {
                    if ( ImageDrop.Visible || Image.Visible ) 
                    {
                        ImageDrop.Visible = false;
                        Image.Visible = true;
                    }
                    else if ( AnimatedImageDrop.Visible || AnimatedImage.Visible )
                    {
                        AnimatedImageDrop.Visible = false;
                        AnimatedImage.Visible = true;
                    }
                }
            }
        }

        public void OnDelayTimeout()
        {
            _canShoot = true;
        }
    }
}
