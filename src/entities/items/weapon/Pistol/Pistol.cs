using Godot;
using UltimateBattle2Die.Core.Interfaces;
using UltimateBattle2Die.Entities.Projectiles;

namespace UltimateBattle2Die.Entities.Items.Weapon.Pistol
{
	public class Pistol : Weapon
    {
        public override void _Ready()
        {
			base._Ready();

            IControllable control = GetComponent<IControllable>();
            
            if ( control != null )
                control.GrabFocus();

            IUsable use = GetComponent<IUsable>();
            if ( use != null )
                use.Use += Use;
        }
    }
}
