using UltimateBattle2Die.Core.Interfaces;

namespace UltimateBattle2Die.Entities.Items.Weapon.Machinegun
{
	public class Machinegun : Weapon
    {
        public override void _Ready()
        {
			base._Ready();

            IControllable control = GetComponent<IControllable>();
            
            if ( control != null )
                control.GrabFocus();

            IUsable use = GetComponent<IUsable>();
            if ( use != null )
                use.Use += Use;
        }
    }
}
