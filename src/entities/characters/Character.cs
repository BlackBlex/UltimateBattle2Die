using Godot;
using UltimateBattle2Die.Core.Interfaces;
using GodotCSTools;
using EntityFramework.Core;
using System;

namespace UltimateBattle2Die.Entities.Characters
{
	public class Character : Entity
    {
		[NodePath("Hands")]
		public Position2D Hands { get; set; }

		public Node CurrentItem { get; set; }

		[Export]
		public int MaxSpeed = 200;

		protected IDamageable damage { get; set; }

		public override void _Ready()
        {
			base._Ready();
			damage = GetComponent<IDamageable>();
        }

        public override void _PhysicsProcess(float delta)
        {
			if ( damage != null )
			{
				if ( !damage.IsAlive )
					return;
			}
			
			base._PhysicsProcess(delta);
        }
    }
}
