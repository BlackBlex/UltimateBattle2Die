using EntityFramework.Core;
using Godot;
using System;
using UltimateBattle2Die.Core.Interfaces;

namespace UltimateBattle2Die.Entities.Characters.Enemy
{
    public class Enemy : Character
    {
		private bool _initial = false;

		public override void Start()
		{
			if ( _initial )
			{
				_initial = false;
	            base.Start();
			}

			if ( damage != null )
			{
				damage.IsAlive = true;
				damage.HP = damage.MaxHP;
				damage.DeadEvents += OnDead;
				damage.DamageReceivedEvents += OnDamageReceived;
			}

			IHittable hit = GetComponent<IHittable>();
			if ( hit != null )
				hit.HitReceivedEvents += OnHitReceived;
		}

		public override void Clean()
		{
			base.Clean();
			_initial = true;
		}

		public override void _Ready()
        {
			base._Ready();
			Start();
		}

        public override void _PhysicsProcess(float delta)
        {
			base._PhysicsProcess(delta);
		}

		public void OnDamageReceived(float damage) {}

		public void OnDead(Entity who) {
			QueueFree();
		}

		public void OnHitReceived(Entity who) {}
    }
}
