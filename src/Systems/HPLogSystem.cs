using System;
using System.Collections.Generic;
using EntityFramework;
using EntityFramework.Core;
using UltimateBattle2Die.Core.Interfaces;
using static EntityFramework.EntityManager;

namespace UltimateBattle2Die.Systems
{
	public class HPLogSystem : ReactiveSystem<Entity>
    {
        public HPLogSystem(EntityManager manager) : base(manager)
        {
        }

        protected override Type ComponentType()
        {
            return typeof(IDamageable);
        }

        protected override Func<Entity, bool> Filter()
        {
            return (entity) => {
                var damage = entity.GetComponent<IDamageable>();
                return damage != null && damage.HP != damage.MaxHP && damage.IsAlive;
            };
            /*var damage = entity.GetComponent<IDamageable>();
            return damage != null && damage.HP != damage.MaxHP && damage.IsAlive;*/
        }

        protected override void Process(float delta, List<Entity> entities)
        {
            foreach ( Entity entity in entities )
            {
                _manager.AddMessage(DebugLocation.BOTTOM, entity.Name + " recibió daño.\n");
            }
        }
    }
}
