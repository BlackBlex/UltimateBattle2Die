using Godot;
using EntityFramework;
using EntityFramework.Core.Interfaces.System;
using UltimateBattle2Die.Entities.Characters.Enemy;
using System.Collections.Generic;

namespace UltimateBattle2Die.Systems
{
	public class RespawnEnemySystem : IProcessSystem
	{
        readonly EntityManager _manager;

        bool _foundEnemy = false;

        float _defaultTime = 10f;
        float _timeLeft;

        public RespawnEnemySystem(EntityManager manager)
        {
            _manager = manager;

            _timeLeft = _defaultTime;
        }

		public void Process(float delta)
		{
            List<Enemy> enemies = _manager.GetEntities<Enemy>();

            _foundEnemy = false;
            
            foreach ( Enemy enemy in enemies)
            {
                if ( enemy.Name.Contains("Enemy") )
                {
                    _foundEnemy = true;
                    break;
                }
            }

            if ( !_foundEnemy && _timeLeft <= 0 )
            {
                Enemy enem = _manager.Create<Enemy>();
                enem.Name = "Enemy";
                enem.Position = new Vector2(300,300);
                _timeLeft = _defaultTime;
            }
            else
                if ( _timeLeft <= 0 )
                    _timeLeft = _defaultTime;
            
            _timeLeft -= delta;
		}
	}
}