using System;
using System.Collections.Generic;
using EntityFramework;
using EntityFramework.Core;
using Godot;
using UltimateBattle2Die.Entities.Characters.Player;
using static EntityFramework.EntityManager;

namespace UltimateBattle2Die.Systems
{
    public class PlayerPositionLogSystem : ReactiveSystem<Player>
    {
        public PlayerPositionLogSystem(EntityManager manager) : base(manager)
        {
        }

        protected override String ComponentTypeString()
        {
            return "Position";
        }

        protected override Func<Player, bool> Filter()
        {
            return (player) => player.Position != Vector2.Zero;
            /*return entity.Position != Vector2.Zero;*/
        }

        protected override void Process(float delta, List<Player> entities)
        {
            foreach ( Player play in entities )
            {
                _manager.AddMessage(DebugLocation.TOP, play.Name + " se movió a: " + play.Position + "\n");
            }
        }
    }
}
