using System;
using System.Collections.Generic;
using EntityFramework;
using EntityFramework.Core;
using Godot;
using UltimateBattle2Die.Core.Interfaces;
using UltimateBattle2Die.Entities.Items.Weapon;
using static EntityFramework.EntityManager;

namespace UltimateBattle2Die.Systems
{
	public class WeaponPickedLogSystem : ReactiveSystem<Weapon>
    {
        public WeaponPickedLogSystem(EntityManager manager) : base(manager)
        {
        }

        protected override Type ComponentType()
        {
        return typeof(IPickable);
        }

        protected override Func<Weapon, bool> Filter()
        {
            return (weapon) => {
                var pickable = weapon.GetComponent<IPickable>();
                return pickable.IsPicked;
            };
            /*var pick = entity.GetComponent<IPickable>();
            return pick.IsPicked;*/
        }

        protected override void Process(float delta, List<Weapon> entities)
        {
            foreach ( Weapon wapon in entities )
            {
                _manager.AddMessage(DebugLocation.BOTTOM, wapon.Name + " fue recogida.\n");
            }
        }
    }
}
