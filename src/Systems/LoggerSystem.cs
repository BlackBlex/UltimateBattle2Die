using EntityFramework;

namespace UltimateBattle2Die.Systems
{
    public class LoggerSystem : SystemManager
    {
        private HPLogSystem HPLogger;
        private WeaponPickedLogSystem weaponPickedLogger;
        private PlayerPositionLogSystem playerPositionLogger;
        
        public LoggerSystem(EntityManager manager) : base()
        {
            HPLogger = new HPLogSystem(manager);
            weaponPickedLogger = new WeaponPickedLogSystem(manager);
            playerPositionLogger = new PlayerPositionLogSystem(manager);
            Add(HPLogger);
            Add(weaponPickedLogger);
            Add(playerPositionLogger);
        }

        ~LoggerSystem()
        {
            Remove(HPLogger);
            Remove(weaponPickedLogger);
            Remove(playerPositionLogger);
        }
    }
}
