namespace UltimateBattle2Die.Core.Interfaces
{
    public interface IDestructible
    {
        int HitsBeforeDestroy { get; set; }
        void Destroy();
    }
}