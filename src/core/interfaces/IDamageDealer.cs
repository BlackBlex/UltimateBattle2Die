namespace UltimateBattle2Die.Core.Interfaces
{
    public interface IDamageDealer
    {
        float DealDamage { get; set; }
    }
}
