using EntityFramework.Core;

namespace UltimateBattle2Die.Core.Interfaces
{
    public interface IDroppable
    {
        Entity ObjOwner { get; set; }

        void Drop();
    }
}