namespace UltimateBattle2Die.Core.Interfaces
{
    public interface IAnimationPlay
    {
        void ProcessAnimation(float delta);
    }
}