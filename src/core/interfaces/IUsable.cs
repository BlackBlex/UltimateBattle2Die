using System;
using EntityFramework.Core;
using Godot;

public delegate void ThingUsed(Entity whichUsed);

namespace UltimateBattle2Die.Core.Interfaces
{
    public interface IUsable
    {
        ThingUsed ThingUsedEvents { get; set; }

        Action Use { get; set; }
    }
}