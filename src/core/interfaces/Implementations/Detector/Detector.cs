using System;
using System.Collections.Generic;
using EntityFramework.Core;
using Godot;
using GodotCSTools;
using UltimateBattle2Die.Components;
using UltimateBattle2Die.Autoloads;
using UltimateBattle2Die.Tools;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.Detector
{
    public abstract class Detector<T> : Component, IDetector<T> where T : Node
    {
        [NodePath("Area/Collision")]
        public CollisionShape2D Collision { get; set; }
        [NodePath("Area/Collision2")]
        public CollisionShape2D Collision2 { get; set; }

        [NodePath("CleanBuffer")]
        public Delay CleanBuffer { get; set; }

		public SortedSet<T> DetectedObjects
        {
            get { return _detectedObjects; }
            set { }
        }
		
        public DetectorDirection Directions
        {
            get; set;
        }
        public DetectorType Type
        {
            get { return _type; }
            set { }
        }
		
        public string ObjectType
        {
            get { return _objectType; }
            set { }
        }
        public float Range
        {
            get { return _range; }
            set { }
        }

        public bool Center
        {
            get { return _center; }
            set { }
        }

        [Export]
        private string _objectType = "";

        [Export]
        private float _range = 2;

        [Export]
        private bool _center = true;

        [Export]
        private DetectorType _type = DetectorType.CIRCULAR;

        [Export]
        private DetectorDirectionDebug _direction = DetectorDirectionDebug.NONE;

        [Export]
        private DetectorDirectionDebug _directionTwo = DetectorDirectionDebug.NONE;

        private SortedSet<T> _detectedObjects = new SortedSet<T>();

        private SortedSet<T> _bufferObjects = new SortedSet<T>();

        private Vector2 _parentSize;

        public override void Start()
        {
            Directions = 0;
            processEnum(_direction);
            processEnum(_directionTwo);
            _parentSize = Parent.Image.GetTexture().GetSize();
            ProcessDetector();
            (Collision.GetParent() as Area2D).CollisionLayer = Parent.CollisionLayer;
            (Collision.GetParent() as Area2D).CollisionMask = Parent.CollisionMask;
            if (!CleanBuffer.IsConnected("timeout", this, "On" + CleanBuffer.Name + "Timeout"))
                CleanBuffer.Connect("timeout", this, "On" + CleanBuffer.Name + "Timeout");
            CleanBuffer.Stop();
        }

		public void OnDetecting(Godot.Object obj)
		{
            if( Filter(obj) )
            {
                _bufferObjects.Add(obj as T);
                _detectedObjects.Add(obj as T);
                if ( CleanBuffer.IsStopped() )
                    CleanBuffer.Start();
            }
		}

		public void OnOutOfRange(Godot.Object obj)
		{
            _bufferObjects.Remove(obj as T);
		}

        public void OnCleanBufferTimeout()
        {
            _detectedObjects.IntersectWith(_bufferObjects);
            CleanBuffer.Stop();
        }

		public abstract bool Filter(Godot.Object obj);

		public void ProcessDetector()
		{
            switch ( Type )
            {
                case DetectorType.CIRCULAR:
                    processCircular();
                    break;
                case DetectorType.LINEAR:
                case DetectorType.DIAGONAL:
                    processLinearDiagonal();
                    break;
                case DetectorType.CROSS:
                    processCross();
                    break;
            }

            if ( !Center && (Type != DetectorType.CIRCULAR && Type != DetectorType.CROSS) )
            {
                RectangleShape2D rect = Collision.Shape as RectangleShape2D;
                if ( rect != null )
                {
                    Vector2 halfSize = rect.Extents;
                    switch ( Type )
                    {
                        case DetectorType.LINEAR:
                            halfSize = halfSize.Rotated(Mathf.Deg2Rad(Collision.RotationDegrees));
                            halfSize += _parentSize * 0.5f;
                            if ( Directions.HasFlag(DetectorDirection.TOP) )
                            {
                                halfSize.x = 0;
                                halfSize.y *= -1;
                                Collision.Position += halfSize;
                            }
                            else if ( Directions.HasFlag(DetectorDirection.BOTTOM) )
                            {
                                halfSize.x = 0;
                                Collision.Position += halfSize;
                            }
                            else if ( Directions.HasFlag(DetectorDirection.LEFT) )
                            {
                                halfSize.x *= -1;
                                halfSize.y = 0;
                                Collision.Position += halfSize;
                            }
                            else if ( Directions.HasFlag(DetectorDirection.RIGHT) )
                            {
                                halfSize.y = 0;
                                Collision.Position += halfSize;
                            }
                        break;
                        case DetectorType.DIAGONAL:
                            halfSize = new Vector2(halfSize.x, halfSize.x);
                            if ( Directions.HasFlag(DetectorDirection.TOP) )
                            {
                                if ( Directions.HasFlag(DetectorDirection.LEFT) )
                                {
                                    halfSize -= _parentSize * 0.50f;
                                    halfSize *= -1;
                                    Collision.Position += halfSize;
                                }
                                if ( Directions.HasFlag(DetectorDirection.RIGHT) )
                                {
                                    halfSize *= 0.5f;
                                    halfSize += _parentSize;
                                    halfSize.y *= -1f;
                                    Collision.Position += halfSize;
                                }
                            }
                            if ( Directions.HasFlag(DetectorDirection.BOTTOM) )
                            {
                                if ( Directions.HasFlag(DetectorDirection.LEFT) )
                                {
                                    halfSize -= _parentSize * 0.50f;
                                    halfSize.x *= -1;
                                    Collision.Position += halfSize;
                                }
                                if ( Directions.HasFlag(DetectorDirection.RIGHT) )
                                {
                                    halfSize *= 0.5f;
                                    halfSize += _parentSize;
                                    Collision.Position += halfSize;
                                }
                            }
                        break;
                    }
                }
            }
            
		}

        private void processEnum(DetectorDirectionDebug directions)
        {
            foreach (DetectorDirection dir in Enum.GetValues(typeof(DetectorDirection)) )
            {
                if ( directions.ToString() == "5" )
                {
                    Directions |= DetectorDirection.ALL;
                    continue;
                }

                if ( dir.ToString() == directions.ToString() )
                {
                    Directions |= dir;
                }
            }
        }

        private void processCircular()
        {
            CircleShape2D circle = new CircleShape2D();
            circle.Radius = Range * DataGame.TileSize;
            Collision.Shape = circle;
        }

        private void processLinearDiagonal()
        {
            RectangleShape2D linear = new RectangleShape2D();
            linear.Extents = new Vector2(Range * DataGame.TileSize, DataGame.TileSize);
            Collision.Shape = linear;

            if ( Directions.HasFlag(DetectorDirection.TOP) || Directions.HasFlag(DetectorDirection.BOTTOM) )
            {
                Collision.RotationDegrees = 90;
            }

            if ( (Directions.HasFlag(DetectorDirection.TOP) && Directions.HasFlag(DetectorDirection.LEFT)) || (Directions.HasFlag(DetectorDirection.BOTTOM) && Directions.HasFlag(DetectorDirection.RIGHT)) )
            {
                Collision.RotationDegrees = 45;
            }
            else if ( (Directions.HasFlag(DetectorDirection.TOP) && Directions.HasFlag(DetectorDirection.RIGHT)) || (Directions.HasFlag(DetectorDirection.BOTTOM) && Directions.HasFlag(DetectorDirection.LEFT)) )
            {
                Collision.RotationDegrees = -45;
            }

        }

        private void processCross()
        {
            RectangleShape2D diagonal = new RectangleShape2D();
            diagonal.Extents = new Vector2(Range * DataGame.TileSize, DataGame.TileSize);
            Collision.Shape = diagonal;
            RectangleShape2D diagonal2 = new RectangleShape2D();
            diagonal2.Extents = new Vector2(Range * DataGame.TileSize, DataGame.TileSize);
            Collision2.Shape = diagonal2;



            if ( Directions.HasFlag(DetectorDirection.TOP) || Directions.HasFlag(DetectorDirection.BOTTOM) )
            {
                Collision2.RotationDegrees = 90;
            }
            else if ( Directions.HasFlag(DetectorDirection.RIGHT) || Directions.HasFlag(DetectorDirection.LEFT) )
            {
                Collision.RotationDegrees = 45;
                Collision2.RotationDegrees = -45;
            }
        }

        public override void _Ready()
        {
            this.SetupNodeTools();
        }
    }
}