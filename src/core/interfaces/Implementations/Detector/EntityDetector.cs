using System.Collections.Generic;
using EntityFramework.Core;
using Godot;
using GodotCSTools;
using UltimateBattle2Die.Components;
using UltimateBattle2Die.Tools;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.Detector
{
	public class EntityDetector : Detector<Entity>
	{
		public override bool Filter(Object obj)
		{
            bool result = false;

            if ( obj is Entity )
            {
                if ( obj.GetType().Name.Contains(ObjectType) )
                    result = true;
            }

            return result;
		}
	}
}