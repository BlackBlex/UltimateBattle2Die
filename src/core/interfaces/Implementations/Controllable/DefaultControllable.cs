using EntityFramework.Core;
using Godot;
using System;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.Controllable
{
	public class DefaultControllable : Component, IControllable
	{
		public Action ProcessControl { get; set; }
		public bool IsActive { get { return _active; } }

		private bool _active = false;

		public override void Process(float delta)
		{
			if ( IsActive )
			{
				if ( ProcessControl != null )
					ProcessControl();
			}
		}

		public void FreeFocus()
		{
			_active = false;
		}

		public void GrabFocus()
		{
			_active = true;
		}

		public async void TransferFocus(Entity newControllable)
		{
            FreeFocus();
			IControllable control = newControllable.GetComponent<IControllable>();
            if ( control != null )
			{
				await newControllable.ToSignal(newControllable.GetTree().CreateTimer(0.1f), "timeout");
                control.GrabFocus();
			}
		}
	}
}
