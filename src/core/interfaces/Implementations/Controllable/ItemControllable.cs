using Godot;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.Controllable
{
	public class ItemControllable : DefaultControllable
    {
        private IPickable pick { get; set; }
        private IDroppable drop { get; set; }

        public ItemControllable() : base() {}

        public override void Start()
        {
            pick = Parent.GetComponent<IPickable>();
            drop = Parent.GetComponent<IDroppable>();

            ProcessControl += DefaultItemControl;
        }

		public void DefaultItemControl()
        {
            if ( pick != null )
            {
                if(pick.ObjOwner != null)
                {
                    if ( !pick.IsPicked )
                    {
                        if ( Input.IsActionPressed("btn_pickup") )
                        {
                            pick.Pickup();
                        }
                    }
                }
            }
            if ( drop != null )
            {
                if ( Input.IsActionPressed("btn_drop") )
                {
                    drop.Drop();
                }
            }
        }
    }
}