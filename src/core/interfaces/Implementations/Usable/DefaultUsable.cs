using System;
using EntityFramework.Core;
using Godot;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.Usable
{
	public class DefaultUsable : Component, IUsable
    {
		public ThingUsed ThingUsedEvents { get; set; }

		public Action Use { get; set; }

        private IControllable _controllable;
        private IPickable _pickable;

		public override void Start()
		{
            if( _controllable == null )
                _controllable = Parent.GetComponent<IControllable>();

            if ( _controllable != null )
            {
                if ( _pickable == null )
                    _pickable = Parent.GetComponent<IPickable>();
                
                if ( _pickable != null )
                    _controllable.ProcessControl += DefaultUseControl;
            }
            else
                throw new Exception("Requires a controllable entity.");
        }

        public override void End()
        {
            if ( _controllable != null && _pickable != null )
                _controllable.ProcessControl -= DefaultUseControl;
        }

		public void DefaultUseControl()
        {
            if ( _pickable.IsPicked )
            {
                if ( Input.IsActionPressed("btn_fire") ) 
                {
                    if ( Use != null )
                        Use();
                }
            }
        }
	}
}
