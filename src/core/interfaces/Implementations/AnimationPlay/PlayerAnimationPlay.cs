using EntityFramework.Core;
using Godot;
using System;
using UltimateBattle2Die.Core.Interfaces;
using UltimateBattle2Die.Entities.Characters;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.AnimationPlay
{
    public class PlayerAnimationPlay : AnimationPlay
    {
        private IMoveable _moveable = null;

        private Character _charact = null;

        public override void Start()
        {
            _moveable = Parent.GetComponent<IMoveable>();
            _charact = Parent as Character;
        }

        public override void ProcessAnimation(float delta)
        {
            if ( _moveable.Direction != Vector2.Zero )
            {
                if ( _charact.Hands.GetChildCount() != 0 )
                {
                    if ( !AnimationComp.CurrentAnimation.Contains("walk_grab") )
                        AnimationComp.Play("walk_grab");
                }
                else
                {
                    if ( !AnimationComp.CurrentAnimation.Contains("walk") )
                        AnimationComp.Play("walk");
                }
            }
            else
            {
                if ( _charact.Hands.GetChildCount() != 0 )
                {
                    if ( AnimationComp.CurrentAnimation.Contains("walk_grab") )
                        AnimationComp.Play("idle_grab");
                }
                else
                {
                    if ( AnimationComp.CurrentAnimation.Contains("walk") )
                        AnimationComp.Play("idle");
                }
            }
        } 
    }
}
