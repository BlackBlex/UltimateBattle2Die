using EntityFramework.Core;
using Godot;
using GodotCSTools;
using System;
using UltimateBattle2Die.Core.Interfaces;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.AnimationPlay
{
    public abstract class AnimationPlay : Component, IAnimationPlay
    {

        [NodePath("../../AnimationPlayer")]
        public AnimationPlayer AnimationComp;

        public abstract void ProcessAnimation(float delta);

        
        public override void _Ready()
        {
            this.SetupNodeTools();
        }

        public override void _PhysicsProcess(float delta)
        {
            ProcessAnimation(delta);
        }
    }
}
