using EntityFramework.Core;
using Godot;
using UltimateBattle2Die.Entities.Projectiles;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.Hittable
{
	public class DefaultHittable : Component, IHittable
	{
		public HitReceived HitReceivedEvents { get; set; }
		public Entity LastEntityHitYou { get; set; }

		public void Hit(Entity who)
		{
			Projectile projectile = who as Projectile;
			if ( projectile != null)
				LastEntityHitYou = projectile.PlayerOwner;
			else
				LastEntityHitYou = who;

			if ( HitReceivedEvents != null ) 
				HitReceivedEvents(who);
		}
	}
}
