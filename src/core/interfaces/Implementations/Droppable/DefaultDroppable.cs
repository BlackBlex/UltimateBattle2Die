using EntityFramework.Core;
using Godot;
using UltimateBattle2Die.Entities.Characters;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.Droppable
{
	public class DefaultDroppable : Component, IDroppable
	{
		public Entity ObjOwner { get; set; }

        public void Drop()
        {
            if ( ObjOwner != null )
            {
                Parent.Position = ObjOwner.GetGlobalPosition();
                IPickable pick = Parent.GetComponent<IPickable>();
                if ( ObjOwner is Character charact )
                {

                    if ( pick != null )
                        Parent.Position += pick.Offset.Rotated(charact.Hands.Rotation);
                    Parent.Rotation = charact.Hands.Rotation;
                    charact.Hands.RemoveChild(Parent as Node);
                    charact.CurrentItem = null;
                }
                
                Node parentObj = ObjOwner.GetParent();
                parentObj.AddChild(Parent);
                if ( pick != null )
                    pick.IsPicked = false;
                
                /*
                    PlayerWeaponry weaponry = ObjOwner.GetComponent<PlayerWeaponry>();
                    weaponry.Weapons.RemoveAt(weaponry.index);
                    weaponry.decreaseIndex();
                    weaponry.weaponSet = false;
                */
                
                ObjOwner = null;
            }
        }
	}
}