using EntityFramework.Core;
using Godot;
using UltimateBattle2Die.Entities.Characters;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.Pickable
{
	public class DefaultPickable : Component, IPickable
	{
		public bool IsPicked
        { 
            get { return _isPicked; } 
            set
            {
                if ( _isPicked == value )
                    return;
                _isPicked = value;
				OnPropertyChanged();
            }
        }

		public Entity ObjOwner { get; set; }
		public Vector2 Offset { get; set; }
		public int Range { get; set; }

        private bool _isPicked;

		public void Pickup()
		{
            IsPicked = true;
            Node parentObj = Parent.GetParent();
            parentObj.RemoveChild(Parent as Node);

            if ( ObjOwner is Character charact )
            {
                charact.Hands.AddChild(Parent);
                charact.CurrentItem = Parent;
            }
            Parent.Position = Vector2.Zero + Offset;
            Parent.Rotation = 0f;

            IDroppable drop = Parent.GetComponent<IDroppable>();
            if ( drop != null )
            {
                drop.ObjOwner = ObjOwner;
            }

            /*
                PlayerWeaponry weaponry = Owner.GetComponent<PlayerWeaponry>();
                weaponry.Weapons.Insert(weaponry.index + 1, this.gameObject);
                weaponry.increaseIndex();
                weaponry.weaponSet = false;
            */
		}
	}
}