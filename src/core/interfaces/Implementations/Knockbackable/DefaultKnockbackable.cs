using EntityFramework.Core;
using Godot;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.Knockbackable
{
	public class DefaultKnockbackable : Component, IKnockbackable
	{
		public int KnockbackStun { get; set; }
		public Vector2 KnockbackDir
		{
			get { return _knockbackDir; }
			set
			{
				if ( _knockbackDir == value )
					return;
				_knockbackDir = value;
				OnPropertyChanged();
			}
		}

        [Export]
        private int MaxKnockbackStun = 10;

        private Vector2 _knockbackDir = Vector2.Zero;

        private IHittable _hittable;

        public override void Start()
        {
            if ( _hittable == null )
                _hittable = Parent.GetComponent<IHittable>();

            if ( _hittable != null )
            {
                _hittable.HitReceivedEvents += ApplyKnockback;
                KnockbackStun = 0;
            }
        }

        public override void Process(float delta)
        {
			if ( KnockbackStun != 0 )
			{
				Parent.MoveAndSlide(KnockbackDir.Normalized() * 50f * 1.5f);
			}

			if ( KnockbackStun > 0 )
            {
				KnockbackStun -= 1;
            }
        }

        public override void End()
        {
            if ( _hittable != null )
                _hittable.HitReceivedEvents -= ApplyKnockback;
        }

		public void ApplyKnockback(Entity origin)
		{
            if ( KnockbackStun == 0 )
            {
                KnockbackStun = MaxKnockbackStun;
                KnockbackDir = Parent.Transform.Origin - origin.Transform.Origin;
            }
		}
	}
}
