using EntityFramework.Core;
using Godot;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.DamageDealer
{
	public class DefaultDamageDealer : Component, IDamageDealer
	{
		public float DealDamage { get; set; }
	}
}
