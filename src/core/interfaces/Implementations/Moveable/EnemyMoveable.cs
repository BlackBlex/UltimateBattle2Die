using EntityFramework.Core;
using Godot;
using System;
using System.Linq;
using UltimateBattle2Die.Entities.Characters;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.Moveable
{
	public class EnemyMoveable : Component, IMoveable
	{
		public Vector2 Direction { get; set; }
		public bool IsRunning { get; set; }

		private int _defaultSpeed = 100;
        
        private IDetector<Entity> _detector;

        private Entity _target;

        public override void Start()
        {
            if ( _detector == null )
                _detector = Parent.GetComponent<IDetector<Entity>>();
            
        }

        public override void Process(float delta)
        {
            if ( _detector.DetectedObjects.Count > 0 )
                DefaultMoveControl();
            else
            {
                _target = null;
                Direction = Vector2.Zero;
            }

            Move(Direction);
        }

        public override void End()
        {
            
        }

		public void Move(Vector2 direction)
		{
            if ( direction != Vector2.Zero )
            {
                int speed = _defaultSpeed;
                Vector2 velocity = direction.Normalized() * speed;
                Parent.MoveAndSlide(velocity);
            }
		}

        private void DefaultMoveControl()
        {
            if ( _target == null && _detector.DetectedObjects.Count > 0 )
                _target = _detector.DetectedObjects.ToList().FirstOrDefault();

            if ( _target != null )
    			Direction = (_target.Position - Parent.Position).Normalized();
            //IsRunning = Input.IsActionPressed("btn_run");
        }
	}
}
