using EntityFramework.Core;
using Godot;
using System;
using UltimateBattle2Die.Entities.Characters;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.Moveable
{
	public class DefaultMoveable : Component, IMoveable
	{
		public Vector2 Direction { get; set; }
		public bool IsRunning { get; set; }

		private int _defaultSpeed = 50;
        private IControllable _controllable;

        public override void Start()
        {
            if ( _controllable == null )        
                _controllable = Parent.GetComponent<IControllable>();

            if ( _controllable != null )
                _controllable.ProcessControl += DefaultMoveControl;
        }

        public override void Process(float delta)
        {
            Move(Direction);
        }

        public override void End()
        {
            if ( _controllable != null )
                _controllable.ProcessControl -= DefaultMoveControl;
        }

		public void Move(Vector2 direction)
		{
            if ( direction != Vector2.Zero )
            {
                int speed = _defaultSpeed;
                //Corregir
                Character character = Parent as Character;
                if ( character != null )
                {
                    speed = (int) (character.MaxSpeed + (Convert.ToInt32(IsRunning) * (character.MaxSpeed / 2.8)));
                }
                //Corregir
                Vector2 velocity = direction.Normalized() * speed;
                Parent.MoveAndSlide(velocity);
            }
		}

        private void DefaultMoveControl()
        {   
			Direction = new Vector2(
                Convert.ToInt32(Input.IsActionPressed("btn_right")) - 
                Convert.ToInt32(Input.IsActionPressed("btn_left")),
                Convert.ToInt32(Input.IsActionPressed("btn_down")) -
                Convert.ToInt32(Input.IsActionPressed("btn_up"))
            );

            IsRunning = Input.IsActionPressed("btn_run");
        }
	}
}
