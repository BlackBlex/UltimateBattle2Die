using EntityFramework.Core;
using Godot;
using GodotCSTools;
using System;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.Destructible
{
    public class DefaultDestructible : Component, IDestructible
    {
		public int HitsBeforeDestroy
        {
            get { return _hitsRemaining; }
            set
            {
				_hitsRemaining = value;
				OnPropertyChanged();
            }
        }

        [NodePath("Animation")]
        private Animation _animation;

        [Export]
        private int _maximumHits = 3;

        private int _hitsRemaining = 0;

		private IHittable _hitParent;
		private IDamageDealer _whoDamageDealer;

		public override void Start()
		{
            _hitsRemaining = _maximumHits;

			if ( _hitParent == null)
				_hitParent = Parent.GetComponent<IHittable>();
			
			if ( _hitParent != null )
				_hitParent.HitReceivedEvents += Subscriber;

            _animation.Current = -1;
            _animation.Visible = false;
            _animation.Position = Parent.Collision.Position;
            _animation.RegionEnabled = true;
		}

		public override void End()
		{
			if ( _hitParent != null )
				_hitParent.HitReceivedEvents -= Subscriber;

            _animation.Visible = false;
		}

		public void Destroy()
		{
            QueueFree();
		}

		private void Subscriber(Entity who)
		{        
            if ( _hitsRemaining > 0 )
            {
                if ( !_animation.Visible )
                {
                    _animation.Visible = true;
                }
                _animation.NextFrame();
                Vector2 colsize = (Parent.Collision.Shape as RectangleShape2D).Extents;
                _animation.RegionRect = new Rect2(new Vector2(32 + (_animation.GetPosFrame() * 64), 0), colsize * new Vector2(1.6f, 2f));
                _whoDamageDealer = who.GetComponent<IDamageDealer>();			
                if ( _whoDamageDealer != null)
                    _hitsRemaining = _hitsRemaining - 1;

                if ( _hitsRemaining == 0 )
                {
                    Parent.QueueFree();
                    End();
                }
            }
        }

        public override void _Ready()
        {
            this.SetupNodeTools();
            _animation.SetMax(_maximumHits);
        }
    }
}
