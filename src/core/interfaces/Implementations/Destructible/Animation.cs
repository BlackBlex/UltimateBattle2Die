using Godot;
using System;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.Destructible
{
    public class Animation : Sprite
    {
        public int Current = -1;
        private int _max;

        private int[][] _process = new int[][]{
            new int[] {0, 0},
            new int[] {0, 0},
            new int[] {2, 4},
            new int[] {0, 2, 4},
            new int[] {1, 2, 3, 4},
            new int[] {0, 1, 2, 3, 4}
        };
        
        public void SetMax(int max)
        {
            _max = max;
        }

        public int GetPosFrame()
        {
            return _process[_max][Current];
        }

        public void NextFrame()
        {            
            if ( _max == 1 )
            {
                Frame = 4;
            }
            else if ( Current < _max )
            {
                Current++;
                Frame = GetPosFrame();
            }
        }
    }
}
