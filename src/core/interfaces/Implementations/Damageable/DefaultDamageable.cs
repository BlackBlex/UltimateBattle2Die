using EntityFramework.Core;
using Godot;

namespace UltimateBattle2Die.Core.Interfaces.Implementations.Damageable
{
	public class DefaultDamageable : Component, IDamageable
	{
		public DamageReceived DamageReceivedEvents { get; set; }
		public Dead DeadEvents { get; set; }

		[Export]
		
		private float EntityMaxHP;

		public float MaxHP { get; set; }
		public float HP 
		{
			get { return _hp; } 
			set
			{
				if ( _hp == value )
					return;
				_hp = value;
				OnPropertyChanged();
			} 
		}
		public bool IsAlive
		{
			get { return _isActive; }
			set
			{
				if ( _isActive == value )
					return;
				_isActive = value;
				OnPropertyChanged();
			}
		}

		private float _hp = 0f;
		private bool _isActive = false;

		private IHittable _hitParent;
		private IDamageDealer _whoDamageDealer;

		public override void Start()
		{
			MaxHP = EntityMaxHP;

			if ( _hitParent == null)
				_hitParent = Parent.GetComponent<IHittable>();
			
			if ( _hitParent != null )
				_hitParent.HitReceivedEvents += Subscriber;
		}

		public override void End()
		{
			if ( _hitParent != null )
				_hitParent.HitReceivedEvents -= Subscriber;
		}

		public void Damage(float damage)
		{
			HP -= damage;

			if ( DamageReceivedEvents != null )
				DamageReceivedEvents(damage);
			
			if ( HP <= 0 )
			{
				if ( _hitParent != null )
				{
					DeadEvents(_hitParent.LastEntityHitYou);
				}
				else
					DeadEvents(null);

				IsAlive = false;
			}
		}

		private void Subscriber(Entity who)
		{
			_whoDamageDealer = who.GetComponent<IDamageDealer>();			
			if ( _whoDamageDealer != null)
			{
				Damage(_whoDamageDealer.DealDamage);
			}
		}
	}
}
