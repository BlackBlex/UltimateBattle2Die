using EntityFramework.Core;
using Godot;
using System;

namespace UltimateBattle2Die.Core.Interfaces
{
	public interface IControllable
	{
		Action ProcessControl { get; set; }
		bool IsActive { get; }

		void FreeFocus();
		void GrabFocus();
		void TransferFocus(Entity newControllable);
	}
}
