using Godot;

namespace UltimateBattle2Die.Core.Interfaces
{
	public interface IMoveable
    {
        Vector2 Direction { get; set; }
        bool IsRunning { get; set; }

        void Move(Vector2 direction);
    }
}