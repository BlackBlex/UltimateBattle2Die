using EntityFramework.Core;
using Godot;

public delegate void DamageReceived(float damage);
public delegate void Dead(Entity whoKilledYou);

namespace UltimateBattle2Die.Core.Interfaces
{
    public interface IDamageable
    {
        DamageReceived DamageReceivedEvents { get; set; }
        Dead DeadEvents { get; set; }

        float MaxHP { get; set; }
        float HP { get; set; }
        bool IsAlive { get; set; }
        
        void Damage(float damage);
    }
}