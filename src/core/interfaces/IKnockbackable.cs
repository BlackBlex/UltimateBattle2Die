using EntityFramework.Core;
using Godot;

namespace UltimateBattle2Die.Core.Interfaces
{
    public interface IKnockbackable
    {
        int KnockbackStun { get; set; }
        Vector2 KnockbackDir { get; set; }

        void ApplyKnockback(Entity origin);
    }
}