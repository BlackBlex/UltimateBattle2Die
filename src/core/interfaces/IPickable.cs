using EntityFramework.Core;
using Godot;

namespace UltimateBattle2Die.Core.Interfaces
{
    public interface IPickable
    {
        bool IsPicked { get; set; }
        Entity ObjOwner { get; set; }
        Vector2 Offset { get; set; }
        int Range { get; set; }

        void Pickup();
    }
}