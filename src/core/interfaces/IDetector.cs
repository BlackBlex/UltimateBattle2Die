using System;
using System.Collections.Generic;
using Godot;
using UltimateBattle2Die.Components;

namespace UltimateBattle2Die.Core.Interfaces
{
    public enum DetectorType {
        CIRCULAR,
        CROSS,
        DIAGONAL,
        LINEAR            
    }

    public enum DetectorDirectionDebug
    {
        NONE,
        TOP,
        RIGHT,
        BOTTOM,
        LEFT,
        ALL = TOP | RIGHT | BOTTOM | LEFT
    }

    [Flags]
    public enum DetectorDirection
    {
        NONE = 1,
        TOP = 1 << NONE,
        RIGHT = 1 << TOP,
        BOTTOM = 1 << RIGHT,
        LEFT = 1 << BOTTOM,
        ALL = TOP | RIGHT | BOTTOM | LEFT
    }

    public interface IDetector<T> where T : Node
    {
        CollisionShape2D Collision { get; set; }
        CollisionShape2D Collision2 { get; set; }

        Delay CleanBuffer { get; set; }

        SortedSet<T> DetectedObjects { get; set; }

        DetectorDirection Directions { get; set; }
        DetectorType Type { get; set; }

        String ObjectType { get; set; }
        float Range { get; set; }

        bool Center { get; set; }

        bool Filter(Godot.Object obj);
        void ProcessDetector();
        void OnDetecting(Godot.Object obj);
        void OnOutOfRange(Godot.Object obj);
    }
}