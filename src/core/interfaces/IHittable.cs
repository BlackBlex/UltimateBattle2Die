using EntityFramework.Core;
using Godot;

public delegate void HitReceived(Entity who);

namespace UltimateBattle2Die.Core.Interfaces
{
    public interface IHittable
    {
        HitReceived HitReceivedEvents { get; set; }

        Entity LastEntityHitYou { get; set; }

        void Hit(Entity who);
    }
}