using EntityFramework.Core;
using Godot;
using System;
using UltimateBattle2Die.Core.Interfaces;

namespace UltimateBattle2Die.Components
{
    public class Shake : Timer, IComponentNode
    {
		public Entity Parent { get; set; }

        [Export]
        public float ShakingTime = 0.25f;
        [Export]
        public float Speed = 1.2f;
        [Export]
        public float Amount = 0.5f;

        [Export]
        private bool _shakeInX = true;
        [Export]
        private bool _shakeInY = false;

        private IHittable _hitParent;

        private Vector2 _parentPosition;

        private Vector2 _resultShake = Vector2.Zero;

        private Random _random = new Random();

        private bool _isShaking = false;

		public override void _Ready()
        {
            SetWaitTime(ShakingTime);

            if ( Parent != null )
            {
                _parentPosition = Parent.Position;
                if ( _hitParent == null )
                    _hitParent = Parent.GetComponent<IHittable>();

                if ( _hitParent != null )
                    _hitParent.HitReceivedEvents += Subscriber;
            }
        }

        public override void _PhysicsProcess(float delta)
        {
            if ( _isShaking )
            {
                if ( _shakeInX )
                    _resultShake.x = (FloatRange(-1.0f, 1.0f) * Mathf.Sin(Speed)) * Amount;
                
                if ( _shakeInY )
                    _resultShake.y = (FloatRange(-1.0f, 1.0f) * Mathf.Sin(Speed)) * Amount;

                Parent.Position += _resultShake;
                
                _resultShake = Vector2.Zero;
            }
        }

		private void Subscriber(Entity who)
        {
            if ( !_isShaking )
            {
                _isShaking = true;
                this.Start();
            }
        }

        private void OnShakeTimeout()
        {
            _isShaking = false;
            Parent.Position = _parentPosition;
        }

        public float FloatRange(float min = 0.0f, float max = 1.0f) {
            return (float) (_random.NextDouble() * (max - min) + min);
        }
    }
}
