using EntityFramework.Core;
using Godot;
using System;

namespace UltimateBattle2Die.Components
{
    public class Trail : Line2D, IComponentNode
    {
        public Entity Parent { get; set; }

        [Export]
        private int trailLenght = 0;
        
        private Vector2 point;

        public override void _Ready()
        {
            if ( Parent != null )
            {
                if ( Parent.Modulate != new Color("#ffffff") )
                {
                    Gradient.SetColor(0, new Color("#00" + Parent.Modulate.ToHtml(false) ));
                    Gradient.SetColor(1, Parent.Modulate);
                }
            }
        }

        public override void _PhysicsProcess(float delta)
        {
            if ( Parent != null )
            {
                GlobalPosition = new Vector2();
                GlobalRotation = 0;
                point = Parent.GlobalPosition;
                AddPoint(point);

                while ( GetPointCount() > trailLenght )
                    RemovePoint(0);
            }
        }

        public override void _ExitTree()
        {
            GlobalPosition = new Vector2();
            GlobalRotation = 0;
            while ( GetPointCount() > 0 )
                RemovePoint(0);
        }
    }
}
