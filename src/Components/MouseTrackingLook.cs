using EntityFramework.Core;
using Godot;
using System;

namespace UltimateBattle2Die.Components
{
    public class MouseTrackingLook : Node, IComponentNode
    {
        public Entity Parent { get; set; }

        private Vector2 _mousePosition = Vector2.Zero;

        public override void _PhysicsProcess(float delta)
        {
            if ( Parent != null )
            {
                _mousePosition = Parent.GetGlobalMousePosition();

                float angleMouse = Mathf.Rad2Deg(Parent.Position.AngleToPoint(_mousePosition));
                
                if ( angleMouse == 180 || angleMouse == 0 )
                    Parent.SpriteStack.Set("angle_deg", angleMouse);
                else 
                    Parent.SpriteStack.Set("angle_deg", -angleMouse);
            }
        }
    }
}
