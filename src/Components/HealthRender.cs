using EntityFramework.Core;
using Godot;
using System;
using UltimateBattle2Die.Core.Interfaces;

namespace UltimateBattle2Die.Components
{
    public class HealthRender : ProgressBar, IComponentNode
    {
		public Entity Parent { get; set; }
        
        IDamageable damage;

        private Color _normalColor = new Color("#0f5803");
        private Color _warningColor = new Color("#d0c415");
        private Color _alertColor = new Color("#af1111");

        private int _normalMark = 0;
        private int _warningMark = 0;
        private int _alertMark = 0;

        private StyleBoxFlat styleBar;

		public async override void _Ready()
        {
            if ( Parent != null )
            {
                await Parent.Manager.WaitForEntitiesProcessed();

                damage = Parent.GetComponent<IDamageable>();
                this.MaxValue = damage.HP;
                this.MinValue = 0;
                this.Value = this.MaxValue;

                styleBar = this.Get("custom_styles/fg") as StyleBoxFlat;

                _normalMark = Mathf.CeilToInt(this.MaxValue);
                _warningMark = (int) ((float)_normalMark * 0.55f);
                _alertMark = (int) ((float)_warningMark * 0.55f);
            }
        }

        public override void _Process(float delta)
        {
            if ( Parent != null )
                if ( damage != null )
                {
                    this.Value = damage.HP;

                    if ( this.Value > _warningMark && this.Value <= _normalMark )
                        styleBar.BgColor = _normalColor;
                    else if ( this.Value > _alertMark && this.Value <= _warningMark )
                        styleBar.BgColor = _warningColor;
                    else
                        styleBar.BgColor = _alertColor;
                }
        }
    }
}
