using EntityFramework.Core;
using Godot;
using System;

namespace UltimateBattle2Die.Components
{
    public class Delay : Timer, IComponentNode
    {
        public Entity Parent { get; set; }

        [Export]
        public float DelayTime = 1f;

        public override void _Ready()
        {
            SetWaitTime(DelayTime);
            if ( Parent != null )
            {
                Connect("timeout", Parent, "On" + Name + "Timeout");
            }
        }
    }
}
