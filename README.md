# UltimateBattle2Die
Juego multiplayer de disparos que tiene diferentes modos de juego.

--------
  
**Nota:**  *Se está utilizando Godot Engine 3.x con C#*

--------

Game | Top-Down Shooter Multiplayer

Juego online

Author: [Twitter](https://twitter.com/blackblex) [Bitbucket](https://bitbucket.org/BlackBlex)

License: General Public License (GPLv3) | http://www.gnu.org/licenses/